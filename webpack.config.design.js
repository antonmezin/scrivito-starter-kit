const builder = require("content-security-policy-builder");
const path = require("path");
const webpack = require("webpack");
const lodash = require("lodash");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { WebpackManifestPlugin } = require("webpack-manifest-plugin");
const Webpackbar = require("webpackbar");
const headersCsp = require("./public/_headersCsp.json");

const BUILD_DIR = "build";

function webpackConfig(_env = {}) {
  return {
    mode: "development",
    context: path.join(__dirname, "src"),
    entry: {
      index: "./design_index.js",
    },
    target: "web",
    module: {
      rules: [
        {
          test: /\.js$/,
          include: [
            path.join(__dirname, "src"),
            path.join(__dirname, "node_modules/autotrack"),
            path.join(__dirname, "node_modules/dom-utils"), // sub-dependency of autotrack
          ],
          use: [
            {
              loader: "babel-loader",
              options: {
                presets: [
                  "@babel/preset-react",
                  [
                    "@babel/preset-env",
                    {
                      debug: false,
                      modules: false,
                      shippedProposals: true,
                      useBuiltIns: "usage",
                      corejs: "3",
                      targets: { browsers: ["defaults"] },
                    },
                  ],
                ],
                cacheDirectory: "tmp/babel-cache",
              },
            },
          ],
        },
        {
          test: /\.s?css$/,
          use: [
            { loader: MiniCssExtractPlugin.loader },
            "css-loader",
            "sass-loader",
          ],
        },
        {
          test: /\.(jpg|png|eot|svg|ttf|woff|woff2|gif)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "assets/[name].[contenthash].[ext]",
              },
            },
          ],
        },
      ],
    },
    output: {
      publicPath: "/",
      filename: (chunkData) =>
        chunkData.chunk.name === "tracking"
          ? "[name].js"
          : "assets/[name].[contenthash].js",
      chunkFilename: "assets/chunk-[id].[contenthash].js",
      path: path.join(__dirname, BUILD_DIR),
    },
    plugins: [
      new Webpackbar(),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: "../public",
            globOptions: { ignore: ["**/_headersCsp.json", "**/_headers"] },
          },
          {
            from: "../public/_headers",
            transform: (content) => {
              const csp = builder({ directives: headersCsp });
              return content
                .toString()
                .replace(/CSP-DIRECTIVES-PLACEHOLDER/g, csp);
            },
          },
        ],
      }),
      new MiniCssExtractPlugin({
        filename: "assets/[name].css",
      }),
      new HtmlWebpackPlugin({
        filename: "design_index.html",
        template: "design_index.html",
        chunks: ["index"],
        inject: false, // needs custom order of script tags
      }),
      new webpack.SourceMapDevToolPlugin({}),
      new WebpackManifestPlugin({ fileName: "asset-manifest.json" }),
    ],
    resolve: {
      extensions: [".js"],
      modules: ["node_modules"],
      symlinks: false,
      fallback: { crypto: false },
    },
    devServer: {
      port: 8090,
      open: true,
      historyApiFallback: { index: "/design_index.html" },
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Security-Policy": devServerCspHeader(),
      },
      devMiddleware: {
        stats: "minimal",
      },
    },
  };
}

function devServerCspHeader() {
  const directives = lodash.cloneDeep(headersCsp);

  // allow 'unsafe-eval' for webpack hot code reloading
  directives["script-src"].push("'unsafe-eval'");

  // allow ws: for webpack hot code reloading
  directives["default-src"].push("ws:");

  return builder({ directives });
}

module.exports = webpackConfig;
