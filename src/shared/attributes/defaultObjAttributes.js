const defaultObjAttributes = {
  body: "widgetlist",
  navigationTitle: "string",
  title: "string",
  showInNavigation: "boolean",
};

const defaultObjProperties = ["title", "navigationTitle", "showInNavigation"];

export { defaultObjAttributes, defaultObjProperties };
