import * as React from "react";
import * as ReactDOM from "react-dom";

import "./assets/stylesheets/index.scss";

function DesignApp() {
  return (
    <div>
      <i className="fa fa-angle-right fa-4" aria-hidden="true" />
      Link to design templates:
      <a href="/design/index.html">HERE</a>
    </div>
  );
}

ReactDOM.render(<DesignApp />, document.getElementById("application"));
