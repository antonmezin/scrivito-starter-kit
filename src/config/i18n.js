import I18n from "i18next";

import editingDE from "./locales/editing/de.json";
import liveDE from "./locales/live/de.json";
import editingEN from "./locales/editing/en.json";
import liveEN from "./locales/live/en.json";

const resources = {
  de: {
    editing: editingDE,
    live: liveDE,
  },
  en: {
    editing: editingEN,
    live: liveEN,
  },
};

I18n.init({
  lng: "de",
  fallbackLng: "de",
  resources,
  debug: false,

  // have a common namespace used around the full app
  ns: ["editing", "live"],
  defaultNS: "live",

  interpolation: {
    escapeValue: false, // not needed for react!!
  },

  // react: {
  //   wait: true,
  // },
});

export default I18n;
