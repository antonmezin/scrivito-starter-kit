import * as Scrivito from "scrivito";
import { validate } from "validate.js";

export function configureScrivito(options) {
  const config = {
    adoptUi: true,
    optimizedWidgetLoading: true,
    strictSearchOperators: true,
    tenant: process.env.SCRIVITO_TENANT,
    constraintsValidation: (constraints) => {
      const name = Object.keys(constraints)[0];
      return (value) =>
        validate({ [name]: value }, constraints, {
          format: "flat",
          fullMessages: false,
        });
    },
  };

  if (process.env.SCRIVITO_ORIGIN) {
    config.origin = process.env.SCRIVITO_ORIGIN;
  }

  if (process.env.SCRIVITO_ENDPOINT) {
    config.endpoint = process.env.SCRIVITO_ENDPOINT;
  }

  if (options?.priority) {
    config.priority = options.priority;
  }

  Scrivito.configure(config);
}
