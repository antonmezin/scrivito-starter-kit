import * as Scrivito from "scrivito";
import I18n from "../../config/i18n";
import { localizeAttrList } from "../../utils/localizeAttrinbute";
import thumbnail from "../../assets/images/thumbnails/Widgets/HeadlineWidget.svg";

Scrivito.provideEditingConfig("HeadlineWidget", {
  title: I18n.t("editing:Widgets.HeadlineWidget.title"),
  description: I18n.t("editing:Widgets.HeadlineWidget.description"),
  titleForContent(obj) {
    return obj.get("title");
  },
  thumbnail,
  attributes: localizeAttrList(["headline"], "HeadlineWidget"),
  properties: ["headline"],
  initialContent: {
    headline: "Lorem Ipsum",
  },
});
