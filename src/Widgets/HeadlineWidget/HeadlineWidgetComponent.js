import * as React from "react";
import * as Scrivito from "scrivito";

import "./HeadlineWidget.scss";

Scrivito.provideComponent("HeadlineWidget", ({ widget }) => (
  <Scrivito.ContentTag
    className="headline-widget"
    content={widget}
    attribute="headline"
  />
));
