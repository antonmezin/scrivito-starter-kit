import * as Scrivito from "scrivito";
import I18n from "../../config/i18n";
import { defaultObjProperties } from "../../shared/attributes/defaultObjAttributes";
import { localizeAttrList } from "../../utils/localizeAttrinbute";
import thumbnail from "../../assets/images/thumbnails/Objs/homepage.svg";
import localizeValidation from "../../utils/localizeValidation";

Scrivito.provideEditingConfig("ContentPage", {
  title: I18n.t("editing:Objs.ContentPage.title"),
  description: I18n.t("editing:Objs.ContentPage.description"),
  titleForContent(obj) {
    return obj.get("title");
  },
  thumbnail,
  attributes: localizeAttrList(defaultObjProperties, "Homepage"),
  properties: [...defaultObjProperties],
  hideInSelectionDialogs: false,
  initialContent: {
    showInNavigation: true,
  },
  validations: [
    [
      "title",
      {
        title: {
          presence: {
            allowEmpty: false,
            message: (_value, attribute) =>
              localizeValidation("presence", attribute),
          },
        },
      },
    ],
  ],
});
