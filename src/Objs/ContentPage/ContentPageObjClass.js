import * as Scrivito from "scrivito";
import { defaultObjAttributes } from "../../shared/attributes/defaultObjAttributes";

const ContentPage = Scrivito.provideObjClass("ContentPage", {
  attributes: {
    ...defaultObjAttributes,
  },
  extractTextAttributes: ["body", "title"],
});

export default ContentPage;
