import * as React from "react";
import * as Scrivito from "scrivito";

Scrivito.provideComponent("ContentPage", ({ page }) => (
  <Scrivito.ContentTag
    tag="div"
    className="content-page"
    content={page}
    attribute="body"
  />
));
