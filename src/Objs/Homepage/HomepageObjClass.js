import * as Scrivito from "scrivito";
import { defaultObjAttributes } from "../../shared/attributes/defaultObjAttributes";

const Homepage = Scrivito.provideObjClass("Homepage", {
  attributes: {
    ...defaultObjAttributes,
  },
  extractTextAttributes: ["body", "title", "navigationTitle"],
});

export default Homepage;
