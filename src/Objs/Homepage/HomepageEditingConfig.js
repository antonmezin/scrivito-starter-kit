import * as Scrivito from "scrivito";
import I18n from "../../config/i18n";
import { localizeAttrList } from "../../utils/localizeAttrinbute";
import { defaultObjProperties } from "../../shared/attributes/defaultObjAttributes";
import thumbnail from "../../assets/images/thumbnails/Objs/homepage.svg";

Scrivito.provideEditingConfig("Homepage", {
  title: I18n.t("editing:Objs.Homepage.title"),
  titleForContent(obj) {
    return obj.get("title");
  },
  thumbnail,
  attributes: localizeAttrList(defaultObjProperties, "Homepage"),
  properties: [...defaultObjProperties],
  hideInSelectionDialogs: true,
});
