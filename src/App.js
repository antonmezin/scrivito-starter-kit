import * as React from "react";
import * as Scrivito from "scrivito";
import { HelmetProvider } from "react-helmet-async";

import CurrentPageMetadata from "./Components/CurrentPageMetadata";
import ErrorBoundary from "./Components/ErrorBoundary";
import NotFoundErrorPage from "./Components/NotFoundErrorPage";
import { CookieConsentProvider } from "./Components/CookieConsentContext";
import CookieConsentBanner from "./Components/CookieConsentBanner";

export const helmetContext = {};

export default function App() {
  return (
    <ErrorBoundary>
      <CookieConsentProvider>
        <HelmetProvider context={helmetContext}>
          <div>
            <div className="content-wrapper">
              <Scrivito.CurrentPage />
              <NotFoundErrorPage />
            </div>
            <CurrentPageMetadata />
            <CookieConsentBanner />
          </div>
        </HelmetProvider>
      </CookieConsentProvider>
    </ErrorBoundary>
  );
}
