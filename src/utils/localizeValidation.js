import I18n from "../config/i18n";

function localizeValidation(constraint, attribute) {
  return I18n.t(`editing:errors.attributes.${constraint}`, {
    attribute: I18n.t(`editing:attributes.common.${attribute}.title`),
  });
}

export default localizeValidation;
