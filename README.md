## Why a starter kit?

Several projects have shown that the following points are important for successful completion:
- HTML templates must exist in their entirety and must be tested by the designer
- Changes to CSS definitions must be incorporated into the development without any further effort
- Components from other projects should be taken over with care
The Scrivito-Example app does not meet these requirements. This starter kit was developed to start a Scrivito project with little effort and with as few dependencies as possible. This kit also offers the possibility of integrating the design templates into the project in such a way that a designer can follow a project and support it with changes.

##  Requirements

- `brew` is installed
   If not: check https://brew.sh/ to install brew on your computer.
- `nvm` is installed  
  `brew install nvm`
- `node` v14.x is installed
  `nvm install v14.17.6`
- `npm` is installed
  normally it is installed with node

### Installation

Now simply call `npm install` in the command line to install all JS dependencies (which are placed in the folder `node_modules/`).

### Design development

Starter Kit have two different http-server configured. One of them is for design purposes.
To start design server you have to start it from you console with
```
  npm run design
```
Overview of design folder is available at `http://localhost:8090/design/index.html`

Under `public/design/example/index.html` is one example file based on bootstrap.
One really important thing is to have css-link pointed to `/assets/index.css`
```
  <link href="/assets/index.css" rel="stylesheet" />
```
It is required for all design pages.

All css definitions have to be done inside of `src/assets/stylesheets`.
You can place you font and images (used in css-definitions) under `src/assets`.

All changes have an immediate effect on the React app. This eliminates the need to transfer the CSS definitions.

#### Preinstalled packages
- bootstrap v5.1.0
- bootstrap-icons v1.5.0

So you don't need to place bootstrap-files inside of stylesheets folder.

#### Fontawesome

File `public/design/index.html` have a example how to use `fontawesome`.
So you can use any glyph-font for you design.


### File structure

- src/assets            : app assets like CSS-files, fonts, images
- src/Components        : general used React Components
- src/config            : app and scrivito configurations
- src/config/locales    : localization files
- src/Objs              : Scrivito Objs
- src/Widgets           : Scrivito Widgets
- src/shared            : shared functions. E.g. attribute definitions for common use
- src/utils             : helper functions
- src/prerenderContent  : Scripts for prerendering of scrivito based content
- src/index.js          : Entry point for React-App
- src/design_index.js   : Entry point for design mode


### Localization
For localization `i18next` is used. Localizer files are placed in `scr/config/locales`.
Two namespaces are defined:
- `editing` : for localization of Scrivito Editor UI
- `live`    : for localization of Live-App
Example for attribute localization can be found in `src/Objs/ContentPage/ContentPageEditingConfig.js`

### Validations in Scrivito
An example of validations is provided in `src/Objs/ContentPage/ContentPageEditingConfig.js`
This localization is based on `validate.js` (https://validatejs.org/).
This library provide a couple of useful constraints for attribute validations.

## How to develop
Before you can start local development copy file `.env.example` to `.env` and
put in a valid Scrivito tenant id instead of `your_scrivito_tenant_id`

```
  SCRIVITO_TENANT=your_scrivito_tenant_id
```
After it call `npm install`.

Calling `npm start` will start a webserver, which listens to [localhost:8080](http://localhost:8080/) and opens this URL in your browser. It should also automatically reload the page after changes to the code (in `src/`) have been made.

### Create new Widgets / Objs

For generating of Objs and Widgets there is a yo-generator customized for given file structure.
For use of this generator please install `yo` (https://yeoman.io/)
```
npm install -g yo
```
